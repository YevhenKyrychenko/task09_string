package com.kyrychenko.views;

import com.kyrychenko.controllers.SentencesController;
import com.kyrychenko.controllers.SentencesControllerImpl;
import com.kyrychenko.regex.RegexTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

public class SentencesView {
    private static Logger logger = LogManager.getLogger(SentencesView.class);
    private SentencesController controller;

    public SentencesView() {
        this.controller = new SentencesControllerImpl();
    }

    public void show(String fileName) throws IOException, URISyntaxException {
        this.controller.loadText(fileName);
        logger.info(this.controller.getSentencesSortedByWordsNumber());
        logger.info(this.controller.findWordAbsentInOtherSentences());
        logger.info(this.controller.findWordsWithGivenLength(4));
        logger.info(this.controller.getAllSortedWordsByFirstLetter());
        logger.info(this.controller.getAllSortedWordsByVowelPercentage());
        logger.info(this.controller.getAllWordsStartsWithVowelSortedByFirstSyllable());
        logger.info(this.controller.sortAllWordsByGivenLetterCount("a"));
        logger.info(this.controller.sortGivenWordsByOccurrence(Arrays.asList("and", "but", "word")));
        logger.info(this.controller.sortAllWordsByGivenLetterCountReverse("a"));

    }
}
