package com.kyrychenko.menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class Menu {
    private static Logger logger = LogManager.getLogger(Menu.class);
    private static Map<Integer, MenuItems> menuItemsMap = new HashMap<>();
    static {
        menuItemsMap.put(0, MenuItems.EXIT);
        menuItemsMap.put(1, MenuItems.EDIT);
        menuItemsMap.put(2, MenuItems.PRINT);
        menuItemsMap.put(3, MenuItems.VIEW);
    }

    public static void main(String[] args) {
        //ResourceBundle bundle = ResourceBundle.getBundle("ApplicationMessages");
        int input;
        Scanner sc = new Scanner(System.in);
        logger.info("Menu");
        logger.info("Choose language: en/de");
        String language = sc.nextLine();
        Locale locale = Locale.ENGLISH;
        if(language.equals("de")) {
            locale = Locale.GERMAN;
        }
        ResourceBundle bundle = ResourceBundle.getBundle("Menu", locale);
        logger.info("/************/");
        do {
            input = sc.nextInt();
            MenuItems menuItems = menuItemsMap.get(input);
            switch (menuItems){
                case EXIT:
                    logger.info(bundle.getString("exit_msg"));
                    break;
                case EDIT:
                    logger.info(bundle.getString("edit_msg"));
                    break;
                case PRINT:
                    logger.info(bundle.getString("print_msg"));
                    break;
                case VIEW:
                    logger.info(bundle.getString("view_msg"));
                    break;
                default:
                    logger.info(bundle.getString("wrong_msg"));
            }

        }while(input != 0);
        sc.close();
    }

}
