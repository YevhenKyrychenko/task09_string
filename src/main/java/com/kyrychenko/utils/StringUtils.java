package com.kyrychenko.utils;

import java.util.Arrays;
import java.util.stream.Collectors;

public final class StringUtils {
    private StringUtils() {
    }

    public static <T> String concat(T ...objects) {
        return Arrays.asList(objects).stream().map(object -> object.toString()).collect(Collectors.joining());
    }
}
