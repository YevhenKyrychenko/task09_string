package com.kyrychenko.models;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

public interface Model {
    List<Sentence> loadText(String fileName) throws URISyntaxException, IOException;

    List<String> getSentencesSortedByWordsNumber();

    Word findWordAbsentInOtherSentences();

    Set<Word> findWordsWithGivenLength(int length);

    List<Word> getAllSortedWordsByFirstLetter();

    List<Word> getAllSortedWordsByVowelPercentage();

    List<Word> getAllWordsStartsWithVowelSortedByFirstSyllable();

    List<Word> sortAllWordsByGivenLetterCount(String letter);

    List<String> sortGivenWordsByOccurrence(List<String> words);

    List<Word> sortAllWordsByGivenLetterCountReverse(String letter);
}
