package com.kyrychenko.models;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Word {
    private final static List<String> VOWELS = Arrays.asList("a", "o", "e", "u", "i");
    private String content;
    private List<String> letters;

    public Word(String content) {
        this.content = content;
        this.letters = Arrays.asList(this.content.split(""));
    }

    public String getContent() {
        return content;
    }

    public String getFirstSyllable() {
        return this.letters.stream()
                .filter(s -> !VOWELS.contains(s))
                .findFirst()
                .orElse(null);
    }

    public Long getGivenLetterCount(String letter) {
        return this.letters.stream()
                .filter(l -> l.equals(letter))
                .count();
    }

    public boolean startWithVowel() {
        return VOWELS.contains(this.letters.get(0));
    }

    public Double getVowelsPercentage() {
        return Long.valueOf(this.letters
                .stream()
                .filter(l -> VOWELS.contains(l))
                .count() / content.length()).doubleValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(content, word.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content);
    }
}
