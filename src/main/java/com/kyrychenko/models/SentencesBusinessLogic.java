package com.kyrychenko.models;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SentencesBusinessLogic implements Model {
    private List<Sentence> sentences;

    @Override
    public List<Sentence> loadText(String fileName) throws URISyntaxException, IOException {
        Path path = Paths.get(getClass().getClassLoader()
                .getResource(fileName).toURI());

        String data = Files.lines(path).collect(Collectors.joining());
        this.sentences = Arrays.asList(data.split("\\.|\\?|\\!")).stream()
                .map(line -> new Sentence(line, convertToWords(line),extractSpliterator(data, line)))
                .collect(Collectors.toList());
        return this.sentences;
    }

    @Override
    public List<String> getSentencesSortedByWordsNumber() {
        return sentences.stream()
                .sorted((s1,s2) -> Integer.valueOf(s1.getWords().size()).compareTo(Integer.valueOf(s2.getWords().size())))
                .map(sentence -> sentence.getOriginalLine())
                .collect(Collectors.toList());
    }

    @Override
    public Word findWordAbsentInOtherSentences() {
        Sentence first = sentences.get(0);
        return first.getWords().stream()
                .filter(word -> !this.sentences.stream()
                        .skip(1)
                        .anyMatch(sentence -> sentence.isWordPresent(word)))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Set<Word> findWordsWithGivenLength(int length) {
        List<Sentence> questions = this.sentences.stream()
                .filter(sentence -> sentence.isQuestion())
                .collect(Collectors.toList());
        return questions.stream()
                .flatMap(sentence -> sentence.getUniqueWordsWithGivenLength(length).stream())
                .collect(Collectors.toSet());
    }

    @Override
    public List<Word> getAllSortedWordsByFirstLetter() {
        return this.sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .sorted((w1,w2) -> w1.getContent().substring(0,1).compareTo(w2.getContent().substring(0,1)))
                .collect(Collectors.toList());
    }

    @Override
    public List<Word> getAllSortedWordsByVowelPercentage() {
        return this.sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .sorted((w1, w2) -> w1.getVowelsPercentage().compareTo(w2.getVowelsPercentage()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Word> getAllWordsStartsWithVowelSortedByFirstSyllable() {
        return this.sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .filter(w -> w.startWithVowel())
                .sorted((w1, w2) -> {
                    if(w1 == null && w2 == null) return 0;
                    if(w1 == null) return -1;
                    if(w2 == null) return 1;
                    return w1.getFirstSyllable().compareTo(w2.getFirstSyllable());
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<Word> sortAllWordsByGivenLetterCount(String letter) {
        return this.sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .sorted((w1, w2) -> {
                    if(w1.getGivenLetterCount(letter) == w2.getGivenLetterCount(letter))
                        return w1.getContent().compareTo(w2.getContent());
                    return w1.getGivenLetterCount(letter).compareTo(w2.getGivenLetterCount(letter));
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<String> sortGivenWordsByOccurrence(List<String> words) {
        return words.stream()
                .sorted((w1, w2) -> getWordOccurrence(w1).compareTo(getWordOccurrence(w2)))
                .collect(Collectors.toList());
    }

    @Override
    public List<Word> sortAllWordsByGivenLetterCountReverse(String letter) {
        return this.sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .sorted((w1, w2) -> {
                    if(w1.getGivenLetterCount(letter) == w2.getGivenLetterCount(letter))
                        return w1.getContent().compareTo(w2.getContent());
                    return w2.getGivenLetterCount(letter).compareTo(w1.getGivenLetterCount(letter));
                })
                .collect(Collectors.toList());
    }

    private List<Word> convertToWords(String line) {
        return Arrays.asList(line.split(" |,|;|-")).stream()
                .map(content -> new Word(content))
                .collect(Collectors.toList());
    }

    private String extractSpliterator(String data, String line) {
        int spliteratorPosition = data.indexOf(line) + line.length();
        return data.substring(spliteratorPosition, spliteratorPosition + 1);
    }

    private Long getWordOccurrence(String word) {
        return this.sentences.stream()
                .mapToLong(sentence -> sentence.getGivenWordOccurrence(word))
                .sum();
    }
}
