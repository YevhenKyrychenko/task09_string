package com.kyrychenko.models;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Sentence {
    private List<Word> words;
    private String originalLine;
    private String spliterator;

    public Sentence(String originalLine, List<Word> words, String spliterator) {
        this.words = words;
        this.originalLine = originalLine;
        this.spliterator = spliterator;
    }

    public Long getGivenWordOccurrence(String word) {
        return this.words.stream()
                .filter(w -> w.getContent().equals(word))
                .count();
    }

    public boolean isWordPresent(Word word) {
        return this.words.stream().anyMatch(word1 -> word1.equals(word));
    }

    public List<Word> getWords() {
        return words;
    }

    public String getOriginalLine() {
        return originalLine;
    }

    public boolean isQuestion() {
        return spliterator.equals("?");
    }

    public Set<Word> getUniqueWordsWithGivenLength(int length) {
        return words.stream()
                .filter(word -> word.getContent().length() == length)
                .collect(Collectors.toSet());
    }
}
