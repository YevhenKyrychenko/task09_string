package com.kyrychenko.controllers;

import com.kyrychenko.models.Model;
import com.kyrychenko.models.Sentence;
import com.kyrychenko.models.SentencesBusinessLogic;
import com.kyrychenko.models.Word;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

public class SentencesControllerImpl implements SentencesController {

    private Model model;

    public SentencesControllerImpl() {
        this.model = new SentencesBusinessLogic();
    }

    @Override
    public List<Sentence> loadText(String fileName) throws URISyntaxException, IOException {
        return this.model.loadText(fileName);
    }

    @Override
    public List<String> getSentencesSortedByWordsNumber() {
        return this.model.getSentencesSortedByWordsNumber();
    }

    @Override
    public Word findWordAbsentInOtherSentences() {
        return this.model.findWordAbsentInOtherSentences();
    }

    @Override
    public Set<Word> findWordsWithGivenLength(int length) {
        return this.model.findWordsWithGivenLength(length);
    }

    @Override
    public List<Word> getAllSortedWordsByFirstLetter() {
        return this.model.getAllSortedWordsByFirstLetter();
    }

    @Override
    public List<Word> getAllSortedWordsByVowelPercentage() {
        return this.model.getAllSortedWordsByVowelPercentage();
    }

    @Override
    public List<Word> getAllWordsStartsWithVowelSortedByFirstSyllable() {
        return this.model.getAllWordsStartsWithVowelSortedByFirstSyllable();
    }

    @Override
    public List<Word> sortAllWordsByGivenLetterCount(String letter) {
        return this.model.sortAllWordsByGivenLetterCount(letter);
    }

    @Override
    public List<String> sortGivenWordsByOccurrence(List<String> words) {
        return this.model.sortGivenWordsByOccurrence(words);
    }

    @Override
    public List<Word> sortAllWordsByGivenLetterCountReverse(String letter) {
        return this.model.sortAllWordsByGivenLetterCountReverse(letter);
    }
}
