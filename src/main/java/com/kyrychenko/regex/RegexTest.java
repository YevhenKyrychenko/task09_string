package com.kyrychenko.regex;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {
    private static Logger logger = LogManager.getLogger(RegexTest.class);

    public static void main(String[] args) {
        String text = "Ceramide you adsa the asdasd";

        String patternString = "\\b[A-Z].*?\\b";

        Pattern pattern = Pattern.compile(patternString);

        Matcher matcher = pattern.matcher(text);
        boolean matches = matcher.matches();
        logger.info(matches);
        splitByWords(text, "the", "you");
        rebaseVowels(text);
    }

    private static void splitByWords(String text, String word1,String word2) {
        Arrays.asList(text.split(word1 + "|" + word2)).stream().forEach(s -> logger.info(s));

    }

    private static void rebaseVowels(String text) {
       logger.info(text.replaceAll("[aeiou](?=.*)", "_"));
    }
}
