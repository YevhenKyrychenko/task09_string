package com.kyrychenko;

import com.kyrychenko.views.SentencesView;

import java.io.IOException;
import java.net.URISyntaxException;

public class Application {
    public static void main(String[] args) throws IOException, URISyntaxException {
        new SentencesView().show("file.txt");
    }
}
